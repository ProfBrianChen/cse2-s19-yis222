public class Test{
  public static void main(String[] args){
    int[] list1 = {1,2,3,4,5,6};
    list1 = reverse(list1);
    
    for(int i = 0; i<list1.length;i++){
      System.out.println(list1[i] + "  " );
    }
  }
  /*public static int[] reverse(int[] list){
    int[] result = new int[list.length];
    for( int i = 0, j = result.length-1; i<list.length; i++,j--){
      result[j] = list[i];
    }
    return result;
  }*/
  
  public static int[] reverse(int[] list){
    for(int i = 0, j = list.length-1; i<list.length;i++,j--){
      int temp = list[i];
      list[i] = list[j];
      list[j] = temp;
    }
    return list;
    /*int temp = list[0];
    list[0] = list[5];
    list[5] = temp;
    return list;*/
  }
}