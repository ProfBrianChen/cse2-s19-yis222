//YiChen Shen
//Jan 27 2019
//CSE 02 HW01 WelcomeClass

public class WelcomeClass{
  
  public static void main(String args[]){
    //Print the welcome to the terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME | ");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-Y--I--S--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("I am learning fun things.");
  }
}