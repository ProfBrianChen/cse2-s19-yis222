//CSE 002 HW08 Yichen Shen
//Program 2: Help the user play the lottery. Choose five integers randomly from 0 to 59, and accept five integers from the user. Determine whetehr the user wins or not.Program
//Learn to use method and arrays
import java.util.Scanner;

public class PlayLottery{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
   

    int[] user = new int[5];
    System.out.print("Enter 5 numbers between 0 and 59: ");
   
    
    for(int i = 0; i<user.length;i++){//accept the values from the user
       while(true){//check whehter the user input an integer
        if(input.hasNextInt() == true){
        break;
        }
      System.out.println("You did not input an integer, please try again:");
      input.next();
    
       
    }
        user[i] = input.nextInt();//accpet the integer from the user
    }
    
    System.out.println();//make another line
    int[] picked = new int [5];
    picked = numbersPicked();//initiate the picked array with numbersPicked
    boolean result = userWins(user,picked);
    if(result == true){//check whether the user wins
      System.out.println("You win!");
    }
    else{
      System.out.println("You lose!");
    }
    
  }
    public static int[] numbersPicked(){
      //genereate five random numbers for the lottery withtout duplication
      System.out.print("The winning numbers are: ");
      int[] numbersPicked = new int[5];
      for(int i =0;i<numbersPicked.length;i++){
        numbersPicked[i] = (int)(Math.random()*60);//generate a random number from 0 to 59, and initiate them in the array
        System.out.print(numbersPicked[i] + " ");
      }
      return numbersPicked;
    }//numbersPicked method
  
   public static boolean userWins(int[] user, int[] winning){//return true when user and winning are the same
     int n = 0;
     
    for(int i = 0; i< user.length;i++){
      for(int j = 0; j < winning.length;j++){
        if(winning[j] == user[i]){
          n +=1;//increase one if there is a matching

        }
      }
    }
     if(n==5){
       return true;//if all numbers are matched, return true
     }
     else{
       return false;//if not all numbers are matched, return false
     }
     
   }//userWins method
    
    
  }
