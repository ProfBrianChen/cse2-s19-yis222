Homework 8 
Total: 95

Program 1: 
+35: Compiles
Compiles!!
+5: filters characters A to M
+5: filters characters N to Z
Filters properly
+5: generates random characters and prints out adequate output.
Good output

Program 2:
+35: Compiles
Compiles!!
+5: Correctly generates a random lottery number with numbers in the right range
Generates proper numbers
+5: Correctly builds an array from the user
Doesn't check if numbers are in range.
+5: checks if the user guessed right, correctly reports to the user if it was right or wrong, checks if the arrays are similar or different.
Checks properly