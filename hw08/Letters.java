import java.util.Random;

public class Letters{
  public static void main(String[] args){
    Random myRandom = new Random();
    //choose an arbitrary number from 1 to 15
    int number = myRandom.nextInt(20);
    char[] letters = new char[number];//declare and allocate an array 
    System.out.println("The arbitrary number of letters is: " + number);
    
    String pool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";//pool for all upper and lower letters
    int randomIndex = 0;
    
    System.out.print("Random characters array: ");
    for( int i=0; i<number;i++){
      randomIndex = myRandom.nextInt(pool.length());
      letters[i] = (pool.charAt(randomIndex));
      System.out.print(letters[i]);
    }
    System.out.println();//make another line
    
    char[] aToM = new char[letters.length];
    char[] nToZ = new char[letters.length];
    aToM = getAtoM(letters);
    nToZ = getNtoZ(letters);
    
    System.out.print("A to M characters: ");//print out a to m characters
   for(int i =0; i<letters.length;i++){
     System.out.print(aToM[i]);
   }
    System.out.println();//make another line
    System.out.print("N to Z characters: ");//print out n to z characters
    for(int i =0; i<letters.length;i++){
     System.out.print(nToZ[i]);
   }
    System.out.println();
   
  
    
  }//main method
  
  
  public static char[] getAtoM(char[] input){
    char[] targetArray = new char[input.length];//delcare the target array
    for(int i = 0; i<input.length;i++){
      
      if(input[i] <= 'm' && input[i] >= 'a' || input[i] <= 'M' && input[i] >= 'A'){//if the letter is betwen a and M or A and M
        targetArray[i] = input[i];
      }
      
    }
    
    return targetArray;//return the target array
  }//getAtoM method
  
  public static char[] getNtoZ(char[] input){
    char[] targetArray = new char[input.length];
    for(int i = 0; i<input.length; i++){
      if(input[i] <= 'z' && input[i] >= 'n' || input[i] <= 'Z' && input[i] >= 'N'){//if the letter is betwen n and z or N and Z
        targetArray[i] = input[i];
      }
    }
    return targetArray;
  }//getNtoZ method
  
}//main class
