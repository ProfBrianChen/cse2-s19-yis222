//CSE002 Yichen Shen HW09 Arrays and method
//Generate random arrays, insert an array into another array, and shorten an array
//Learn how to use method and arrays

import java.util.Scanner;//import the scanner class

public class Manipulation{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);//declare the scanner
    System.out.print("Enter 1 for insert; Enter 2 for shorten: ");//prompt the user to choose to insert or shorten
    int user = myScanner.nextInt();//accept the input from the user
    
    if(user == 1){//insert
      int[] input1 = generate();//generate the random array
      int[] input2 = generate();//generate another random array
      int[] arrayFinal = insert(input1, input2);
    
      System.out.println("Input 1:{" + print(input1) +"} "+ "  Input 2 :{" + print(input2) + "}");//print out the input 1 and input 2
      System.out.println("Output:{" + print(arrayFinal) + "}");//print out the final output
    }
    else{
      int[] input1 = generate();//generate the random array
      int randomInput = (int)(Math.random()*26);//generate a random input
      int[] arrayFinal = shorten(input1,randomInput);
      System.out.println("Input 1:{" + print(input1) +"} "+ "  Input 2 : " + randomInput);//print out the input 1 and input 2
      System.out.println("Output:{" + print(arrayFinal) + "}");//print out the final output
    }
    
  }//main method
  
  public static int[] generate(){
    int random = (int)(Math.random()*11+10);//generate a random number between 10 and 20
    int[] array = new int[random];//declare and allocate random size of memory to the array
    for( int i = 0; i<array.length; i++){
      array[i] = (int)(Math.random()*26);//generate random integers from 0 to 25
    }
    
    return array;//return the array
  }//genereate method
  
  public static int[] insert(int[] x, int[] y){
    int length = x.length+y.length;//length of the new array by adding two length of the input array together
    int[] newArray = new int[length];
    int random = (int)(Math.random() * x.length);//generate a random index for the first array
    System.out.println("The random index to insert is " + random);
    int j = 0; //index for the first array
    int k = 0;//index for the second array
    
    for(int i = 0; i<newArray.length; i++){
      
      if(i<=random){//if i is less than the random index
        newArray[i] = x[j];
        j++;//increase one to j
      }
      else if( i>random && (i-random)<=y.length){//if i is larger than the index and i minus the random index is smllaer than the length of the second array
        newArray[i] = y[k];
        k++;//increae j by one
      }
      else{//if i is larger than the random index and the length of the second array
        newArray[i] = x[j];
        j++;//increae       
      }   
    }//for loope
    
    return newArray;//return the new array
    
  }//insert mothod
  public static int[] shorten(int[] input, int index){
    int[] newArray = new int[index+1];//declare and initialize a new array
    if(index>input.length-1){//if the input integer is not withtin the range of the  index of the array
      return input;//return the original array
    }
    else{//if the input integer is withtin the range of index of the array
      for(int i=0; i<=index; i++){
        newArray[i] = input[i];
      }
      return newArray;//return the new array
    }
    
  }//shorten method
  
  public static String print(int[] input){//print out the member of values in an array
    String x = "";
    for( int i = 0; i<input.length; i++){
      x += input[i] + "";
      if(i<(input.length-1)){//if this value is not the last member
        x += ",";//add a ,
      }
    }
    return x;
   
  }//print method
  
}//main class