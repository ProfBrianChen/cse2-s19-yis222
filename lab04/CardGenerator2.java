//
public class CardGenerator2{
  public static void main(String args[]){
    int randomNumber = (int)(Math.random()*51)+1;//generate the random number from 1 to 51
    String identity = "";
    String suit = "";
    
    
    //identity their suit
    if(randomNumber <= 13){
      suit = "Diamonds";
    }
    else if (randomNumber <=26){
      suit = "Clubs";
    }
    else if (randomNumber <=39){
      suit = "Hearts";
    }
    else if(randomNumber <= 52){
      suit = "Spades";
    }
    randomNumber %= 13; //convert into 1-13
    
    switch(randomNumber){
      case 1:
        identity = "Ace";
        break;
      case 11:
        identity = "Jack";
        break;
      case 12:
        identity = "Queen";
        break;
      case 13: 
        identity = "King";
        break;
      default:
        identity = (String)randomNumber;
        break;
    }
    
    System.out.println("You picked the " + suit + " of " + identity);
  }
}