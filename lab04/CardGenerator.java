//Yichen Shen Feb,15,2019
//CSE002 LAB04
//Display a card with number and shapes randomly
//Learn how to use if, switch statement and Math.random(),and modulus operator

import java.lang.Math; //Import the math class to generate a random number

//class
public class CardGenerator{
  //main method
  public static void main(String[] args){
    int randomNum = (int)(Math.random()*51)+1;//generate the random number from 1 to 52, and cast it into an integer
    String suit = "suit";
    String identity = "identity";
    int cardIdentity = 0;
    
    if(randomNum <= 13){
      suit = "Diamonds"; //if the number is less than 13, suit is assigned as diamonds
      cardIdentity = randomNum; //assign card number to the numIdentity
    }
    else if(randomNum >= 14 && randomNum <= 26){
      suit = "Clubs"; //if the number is between 14 and 26, suit is assigned as clubs
      cardIdentity = randomNum % 13; //Conver the higher number into royal cards
    }
    else if(randomNum >= 27 && randomNum <=39){
      suit = "Hearts"; //if the number is between 27 and 39, suit is assigned as hearts
      cardIdentity = randomNum % 13; //Conver the higher number into royal cards
    }
    else if(randomNum >=40 && randomNum <= 52){
      suit = "Spades"; //if the number is between 40 and 52, suit is assigned as spade
      cardIdentity = randomNum % 13; //Conver the higher number into royal cards
    }
    
    switch (cardIdentity) {
      case 1:
      identity = "Ace"; //if royal cards euqals 1, assign Ace to identity
        break;
      case 11:
        identity = "Jack";//if royal cards euqals 11, assign Jack to identity
        break;
      case 12:
        identity = "Queen";//if royal cards euqals 12, assign Queen to identity
        break;
      case 0:
        identity = "King";//if royal cards euqals 13, assign King to identity
        break;
      default:
        identity = ""; //if royal number is none of those above, assign nothing to identity
    }
    
    if (cardIdentity == 1 || cardIdentity == 11 || cardIdentity == 12 || cardIdentity == 0){ //if identity equals 1 or 11 or 12 or 13, print out the special identity
    System.out.println("You picked the " + identity + " of " + suit);
    }
    else { //if identity is not those above, print out the number directly
      System.out.println("You picked the " + cardIdentity + " of " + suit);
    }
    
  }//end of main method
}//end of class