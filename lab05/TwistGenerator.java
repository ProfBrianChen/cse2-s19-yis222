//Yichen Shen Lab05
//Ask the user to input an integer as the length, and print out a simple "twist" on the screen
//Learn how to use loop

import java.util.Scanner; //import scanner 

public class TwistGenerator{
  public static void main(String[] agrs){
    //Ask the user to input an integer
    Scanner myScanner = new Scanner(System.in);  //declare a scanner instance
    System.out.print("Enter an integer as the length of the twist: "); //prompt the user to input their answer
    
    while (myScanner.hasNextInt() == false  ){ //if the user did not input an integer
      System.out.print("You did not enter an integer, please enter an integer again: "); //propmt the user try again
      myScanner.next(); //flush out the junk word
      
    }
    
   while(myScanner.nextInt() < 0){
    System.out.print("You did not enter an integer, please enter an integer again: "); //propmt the user try again
      myScanner.next(); //flush out the junk word 
   }
    
    int length = myScanner.nextInt(); //assign the integer to length
    int position1 = 1; //position index for the first line
    int position2 = 1; //position index for the second line
    int position3 = 1; //position index for the third line
   
  
    
    int n = 1;
    
    while(position1 <= length){ //print out the first line of the twist
      n = position1 % 3;
      if(n == 1){
        System.out.print("\\");
      }
      else if(n == 2){
        System.out.print(" ");
      }
      else if(n == 0){
        System.out.print("/");
      }
      position1 +=1;
    }
    
    System.out.println(""); //next line
    
    while(position2 <= length){//print out the second line of the twist
      n = position2 % 3;
      if(n == 1){
        System.out.print(" ");
      }
      else if(n == 2){
        System.out.print("X");
      }
      else if(n == 0){
        System.out.print(" ");
      }
      position2 +=1;
    }
      
    System.out.println(""); // next line
    
     while(position3 <= length){//print out the third line of the twist
      n = position3 % 3;
      if(n == 1){
        System.out.print("/");
      }
      else if(n == 2){
        System.out.print(" ");
      }
      else if(n == 0){
        System.out.print("\\");
      }
      position3 +=1;
    }
    
    //System.out.println("Your length is : " + length);
    
  }
}