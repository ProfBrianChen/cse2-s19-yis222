//CSE002 LAB07 Yichen Shen March 22 2019
//generate an artificial story by using methods and loops

import java.util.Random;//import Random
import java.util.Scanner;//import the scanner

public class StoryGenerator{
  public static void main(String[] args){
   
    story();
        
  }
  
  //final moethod to wrap them up together, and generate a random number of action sentences
  public static void story(){
    String subject = "";
    String adjective = "";

    //generate the first sentence
    subject = thesisSentence();
    
    Random randomGenerator = new Random();//create the random object
    int randomInt = randomGenerator.nextInt(10);//generate random integers less than 10
    for(int i = 1; i<randomInt; i++){
      actionSentence(subject);
    }
        System.out.println("");//make another line

    finalSentence(subject);
   // System.out.println(randomInt);
    
  }
  
  //generate a final sentence after the action sentence
  public static void finalSentence(String subjectFromTheThesis){
    String n1 = subjectFromTheThesis; //get the subject
    System.out.println("That " + n1 + " " + verb() + " her " + none2() + ". ");//generate the final sentence
  }
  
  //phase 2: generate an action sentence after the first thesis sentence
  public static void actionSentence(String subjectFromTheThesis){
    String n1 = subjectFromTheThesis;//get the subject
    System.out.print("The " + n1 + " used " + none2() + " to " + verb() + " " + none2() + " at the " + adjective() + " " + none2() + ". ");
    
  }
  
  //phase 1: generate a  thesis sentence
  public static String thesisSentence(){
    String n1 = "";
    boolean ifContinue = true;
    
    Scanner input  = new Scanner(System.in);//declare the scanner
    while(ifContinue){
     n1 = none1();

      System.out.print("The " + adjective() + " " + n1 + " " + verb() + " " + adjective() + " " + none2() + ". ");
      System.out.println("");
      //ask the user if it is satisfied
      System.out.print("If you would like another sentence? yes-->1 /no-->2: ");
      int answer = input.nextInt();//get the answer from the user
      if(answer == 2){//if the user does not want another sentence
       break;
      }
    }
    return n1;
  }
  
  //Phase 0: generate four basic kinds of words
  
  //1. generate an adjectives
  public static String adjective(){
    Random randomGenerator = new Random();//create the random object
    int randomInt = randomGenerator.nextInt(10);//generate random integers less than 10
    switch(randomInt){
      case 1:
      return "fast";
      case 2:
      return "yellow";
      case 3:
      return "silly";
      case 4:
      return "fun";
      case 5:
      return "doglike";
      case 6:
      return "beautiful";
      case 7:
      return "furry";
      case 8:
      return "cold";
      case 9:
      return "silver";
      default:
      return "cute";
        
    }
  }
  public static String verb(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt){
      case 1:
        return "ate";
      case 2:
        return "killed";
      case 3:
        return "helped";
      case 4: 
        return "admired";
      case 5: 
        return "crashed";
      case 6:
        return "copied";
      case 7:
        return "baked";
      case 8:
        return "stole";
      case 9:
        return "bought";
      default:
      return "inspired";
    }
  }
  
  public static String none1(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt){
      case 1:
        return "professor";
      case 2:
        return "cat";
      case 3:
        return "dog";
      case 4: 
        return "monkey";
      case 5: 
        return "student";
      case 6:
        return "hero";
      case 7:
        return "man";
      case 8:
        return "baby";
      case 9:
        return "Sun";
      default:
      return "Moon";
    }
    
  }
  public static String none2(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch(randomInt){
      case 1:
        return "audience";
      case 2:
        return "broomstick";
      case 3:
        return "owl";
      case 4: 
        return "student";
      case 5: 
        return "test";
      case 6:
        return "ring";
      case 7:
        return "gold";
      case 8:
        return "computer";
      case 9:
        return "cup";
      default:
      return "pencil";
    }
  }
  
 
  
}