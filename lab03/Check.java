//Yichen Shen Feb 08 2019
//CSE002 LAB03
//Objectives: How to get input from the user and use the data to perform basic computations
//Determine how much each person needs to pay the bill evently by obtaining input from the user
//Input includes original cost of the check, the percentage tip and the number of ways the check will be split

import java.util.Scanner;

public class Check{
  //main method required for every Java program
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in ); //declare and construct an instance of the Sanner object
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompt the user for the original cost of the check
    double checkCost = myScanner.nextDouble(); //declare and accept user input
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");//prompt the user for the tip percentage
    double tipPercent = myScanner.nextDouble(); //declare and accept user input
    
    tipPercent /=100 ; //convert the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); //prompt the user for the number of people 
    int numPeople = myScanner.nextInt();//declare and accept user input
    
    double totalCost; //declare the total cost
    double costPerPerson; //declare the cost per person
    int dollars, //whole dollar amount of cost
    dimes, pennies; //for storing digits to the right of the decimal point for the cost$
    totalCost = checkCost * (1+tipPercent); //Calculate the total cost by multpiying check cost and 1 plus tip percentage
    costPerPerson = totalCost/numPeople; //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g.,
    //(int)(6.73 * 10) % 10 -> 67%10 -> 7
    //where the % (mod) operator returns the remainder
    //after the division: 583%100 -> 83, 27%5 ->2
    dimes =(int)(costPerPerson*10)%10; //find the dimes
    pennies=(int)(costPerPerson*100)%10; //find the pennies 
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);    
    
    
  } //end of main method
} //end of class