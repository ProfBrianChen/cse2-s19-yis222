//Yichen Shen Feb,6,2019
//CSE002 HW02
//Compute the cost of the items during the shopping, including taxes of 6%

public class Arithmetic{
  //main method required for every java program
  public static void main(String[] args){
    //input data
    //Number of pairs of pants
    int numPants=3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //Cost per belts
    double beltCost = 33.99;
    
    //the tax rate in the PA
    double paSalesTax = 0.06;
    
    //intermediate data
    double totalCostOfPants; //total cost of pants
    double totalCostOfShirts; //total cost of shirts
    double totalCostOfBelts; //total cost of belts
    
    double taxOfPants; //sales tax charged on pants
    double taxOfShirts; //sales tax charged on shirts
    double taxOfBelts; //sales tax charged on belts
    
    double totalCostOfPurchases; //total cost of purchases before tax
    double totalSalesTax; //Total sales tax on all the items
    double totalPaid;//Total paid for this transaction, including sales tax
    
    //calculation
    //for each total cost of an item, number of items times the price per item
    totalCostOfPants = numPants * pantsPrice;//Gives the total cost of pants before tax
    totalCostOfShirts = numShirts * shirtPrice; //Gives the total cost of shirts before tax
    totalCostOfBelts = numBelts * beltCost; //Gives the total cost of belts before tax
    
    //for the tax of each item, total cost of each item times the tax rate in the PA
    //for the tax of each item,the original double value times 100 and converts to an integer explicitly
    //and convert to double implicitly, so that the answer would only contain at most two digital numbers, which makes sense as money values
    taxOfPants = (int)(totalCostOfPants * paSalesTax * 100);//convert into an int
    taxOfPants /=100;//Gives the tax on all pants
    taxOfShirts = (int)(totalCostOfShirts * paSalesTax * 100);//convert into an int
    taxOfShirts /=100;//Gives the tax on all shirts
    taxOfBelts = (int)(totalCostOfBelts * paSalesTax * 100);//convert into an int
    taxOfBelts /=100;//Gives the tax on all belts
    
    //add total cost of each item together before the tax
    totalCostOfPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;//Gives the total cost of purchases befroe the tax
    //add tax of each items together
    totalSalesTax = taxOfPants + taxOfShirts + taxOfBelts; //Gives the total tax of all the item
    //add total cost of purchases before the tax and total sales tax together
    totalPaid = totalCostOfPurchases + totalSalesTax; //Gives the total paid for this transaction
    
    //print out the final vales
    System.out.println("The total cost of pants before the tax are " + totalCostOfPants +" dollars.");
    System.out.println("The total cost of shirts before the tax are " + totalCostOfShirts+" dollars.");
    System.out.println("The total cost of belts before the tax are " + totalCostOfBelts+" dollars.");
    System.out.println("The tax charged on all pants are "+taxOfPants+" dollars.");
    System.out.println("The tax charged on all shirts are "+taxOfShirts+" dollars.");
    System.out.println("The tax charged on all belts are "+taxOfBelts+" dollars.");
    System.out.println("The total cost of purchases before the tax are "+totalCostOfPurchases+" dollars.");
    System.out.println("The total tax charged on all items are "+totalSalesTax+" dollars.");
    System.out.println("The total paid for this transaction, including the tax, are "+totalPaid+" dollars.");
    
    
  } //end of main method
} //end of class