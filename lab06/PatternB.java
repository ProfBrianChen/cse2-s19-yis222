//Yichen Shen March 08, 2019
//CSE02 Lab06 
//Pattern B
//Use loops to form a pattern

import java.util.Scanner; //import the scanner class

public class PatternB{
  //main class 
  public static void main(String[] args){
    //main method
    //ask the input from the user
    Scanner input = new Scanner(System.in);//declare an instance for the scanner
    System.out.print("Enter the size of the pyramid, from 1 to 10: ");//ask the user to input
    int userNumber = 0;//declare and assign the input from the user
    
    //check whether the user input the correct number
    while(true){
      while(input.hasNextInt() == false){
      System.out.print("You did not enter an integer, please try again: ");//ask the user again if the user did not input an integer
      input.next();//flush out the junk data
      }
      
      userNumber = input.nextInt();//get the integer from the scanner
      
      if(userNumber>=1 && userNumber<=10){//if the user did not input an integer within 1 and 10
        break;
      }
      
      System.out.print("The number is not between 1 and 10, please try again: ");//ask the user again 
      //input.next();//flush out the negative integer
      
    }
    
    
    //Make the pattern B
    int temp = userNumber;
    for(int numRow = 1; numRow <= userNumber; numRow++){
      for(int i = 1; i<= temp; i++){
        System.out.print(i+" ");
      }
      System.out.println("");//make another line
      temp -=1;//minus one to the temp
      
    }
    
    
  }
}
    