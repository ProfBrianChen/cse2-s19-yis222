//CSE002 Hw10 robot city
//multidimentional arrays
//build city blocks, assign populations, make robots to destroy people, and update to destroy more

import java.util.Scanner;

public class RobotCity{
  public static void main(String[] args){
      Scanner input = new Scanner(System.in);
    System.out.print("Enter the number of robtos: ");
    int k = input.nextInt();
    int[][] x = buildCity();
    System.out.println("Before invade:");
    display(x);
    System.out.println("After invade:");
    invade(x,k);//make five robots in the city blocks
    display(x);
    for(int i =1; i<=5; i++){//update and displayfor five times      
      System.out.println("Update for the  " + i + " time");
      update(x);
      display(x);
    }
  }
  
  public static int[][] buildCity(){
    
    int rows = (int)(Math.random()*6+10);//randomly generate east-west dimentions
    int columns = (int)(Math.random()*6+10);//randomly generate south-north dimentions
    int[][] city = new int[rows][columns];
    
    for(int i = 0; i < city.length; i++){
      for(int j = 0; j<city[i].length; j++){
        city[i][j] = (int)(Math.random()*900+100);//assign a random number between 100 and 999 to represent populations
      }
    }//for i loop
    return city;
  }//buildCity method
  
  public static void display(int[][] cityArray ){//a method to display the two dimentional array
    for(int i = 0; i<cityArray.length;i++){
      for(int j = 0; j<cityArray[i].length;j++){
        System.out.printf("%d\t",cityArray[i][j]);
      }
      System.out.println();
    }
  }
  
  public static void invade(int[][] cityArray, int k){
    for(int i = 0; i < k; i++){//generate k robots
      int x = (int)(Math.random()*cityArray.length);//generate a random x coordinate
      int y = (int)(Math.random()*cityArray[0].length);//generate a radnom y coordinate
      
      while(cityArray[x][y] < 0 ){//if there has been one robot there before, choose anothe coordinate
      x = (int)(Math.random()*cityArray.length);
      y = (int)(Math.random()*cityArray[0].length);
      }
      cityArray[x][y] = 0-cityArray[x][y];
    }//for loop
  }//invade method
  
  public static void update(int[][] cityArray){//moves the robots to the next block eastwards 
    for(int i = cityArray.length-1; i>=0; i--){
      for(int j = cityArray[i].length-1; j>=0; j--){
        if(cityArray[i][j] < 0 && j<cityArray[0].length-1){
          cityArray[i][j+1] = 0 - cityArray[i][j+1]; //manipulate the negative value
          cityArray[i][j] = 0 - cityArray[i][j];
        }
        if(cityArray[i][j]< 0 && j ==cityArray[0].length-1){
          cityArray[i][j] = 0 - cityArray[i][j];
        }
        
      }
    }
    
  }//update method
  
}//main class