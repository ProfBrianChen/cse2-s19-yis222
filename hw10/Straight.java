//CSE002 HW10 Straight Yichen Shen
//single dimentional array, sort
//make a method to create and shuffle a deck of cards, draw the first five cards without repeating, sort them, and detect whether they are a straight


public class Straight{
  public static void main(String[] args){
    int n = 0;
   for(int i = 0; i<1000000; i++){
    int[] newArray = shuffle();//make a deck of shuffled cards
    int[] hands = drawCards(newArray);//draw the first five cards from the shuffled deck
      if(straight(hands) == true){//if the five are straight
        n++;
      }
    }
    
    double x = ((double)n)/1000000;
    System.out.println("Number of straight in one million times: " + n + " Percentage: " + x*100 + "%");
  }
  
  
  
  public static int[] shuffle(){
    int[] a = new int[52];
    for(int i = 0; i<a.length;i++){//assign 0-51 to an array
      a[i] = i;//assign exact values to the array
      if(a[i]%13 == 0){a[i] = 13;}
      else{a[i] = a[i]%13;}//module by 13 to get the values between 0 and 12
      
    }
    
    for(int i = 0; i<a.length;i++){//shuffle deck of cards
      int j = (int)(Math.random()*52);
      int temp = a[i];
      a[i] = a[j];
      a[j] = temp;
    }
    return a;
    
  }//shuffle method
  
  public static int[] drawCards(int[] a){//draw first five cards from the shuffled deck, check whether there is any repetition
    int[] x = new int[5];//make another array
    int n = 0;
    //This part is to eliminate the conditions where two values would be the same for the first five cards in a shuffeld deck of cards;
    //But the result woule be around 0.7168%, which is farther away from the answer
    /*
    while(true){
      n = 0;
    for(int i = 0; i<x.length;i++){
      for(int j = i+1; j<x.length;j++){
        if(a[i] == a[j]){
          n++;
        }
      }
    }
      if(n==0){break;}
       a=shuffle();
    }*/ 
    
    for(int i = 0; i<x.length;i++){//copy the values from the shuffled deck of cards to another new array
      x[i] = a[i];
    }
    return x;
  }
  
  public static int search(int[] a, int k){//search for the kth lowest card
    if(k>5||k<=0){
      return -99999;//return an error if k is out of the range 
    }
    //sort the values first
    int minIndex = 0;
    int min = 99;
    for(int i = 0; i<a.length; i++){
      minIndex = i;
      min = a[i];
      for(int j = i; j<a.length;j++){//finding the minimum value
        if(a[j] < min){
          min = a[j];
          minIndex = j;
        }
      }
      
      if(minIndex != i){ //if the index of minimum does not equal to i, swap two values
      int temp = a[i];
      a[i] = a[minIndex];
      a[minIndex] = temp;
      }
      
     }
    int result = a[k-1];
    return result;
    }
  
  public static boolean straight(int[] x){//if the five cards in the array are a straight, return true, otherwise return false
    if(search(x,1) == search(x,2)-1 && search(x,2) == search(x,3)-1 && search(x,3) == search(x,4)-1 && search(x,4) == search(x,5)-1){
      return true;
    }
    return false;
  }
  
  public static void display(int[] a ){
    for(int i = 0; i<a.length; i++){
      System.out.println(a[i]);
    }
  }
  
}//main class