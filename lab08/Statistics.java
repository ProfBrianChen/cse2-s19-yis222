//CSE 002 LAB 08 Yichen Shen
//Arrays!
import java.util.Arrays;
import java.util.Random;

public class Statistics{
  public static void main(String[] args){
    Random randomGenerator = new Random();
    int randomInt = 0;
    int number = 52;
    int[] list;//declaration of an array
    list = new int[number];//allocation of memory
    System.out.println("The size of the array is : " + list.length);//print out the length of the array
    
    for(int i = 0; i<list.length; i++){
      list[i] = randomGenerator.nextInt(100);//initialize the array with a random number
      System.out.println("list[" + i + "]" + " = " + list[i]);//print out the number
    }
    
    int range = getRange(list);
    System.out.println("Range: " + range);
    double mean = getMean(list);
    System.out.println("Mean: " + mean);
    double std = getStdDev(list,mean);
    System.out.println("Standard deviation: " + std);
    //System.out.println(list);
    shuffle(list);
    
  }//main method
  
  public static int getRange(int[] array){//get the range of an array
    int range = 0 ;
    Arrays.sort(array);//sort the array
    range = array[array.length-1] - array[0];//range = maximum value - minimum value
    return range;
  }
  
  public static double getMean(int[] array){
    int sum = 0;
    int mean = 0;
    for(int i = 0; i<array.length; i++){
      sum += array[i];//add all values in an array together
    }
    mean = sum/array.length;//use sum divided by the total number of values in the array
    return mean;
  }
  
  public static double getStdDev(int[] array, double mean){
    int temp = 0;
    double stdDev = 0;
    for(int i = 0; i<array.length; i++){
      temp += Math.pow((array[i] - mean),2);
    }
    stdDev = Math.sqrt(temp/(array.length-1));
    return stdDev;
    
    
  }
  
  public static void shuffle(int[] array){
    int j = 0;
    int temp = 0;
    System.out.println("Array after shuffling: ");
    for(int i = 0; i<array.length;i++){
      j = (int)(Math.random() * array.length);
      //swap two numbers of array[i] and array[j]
      temp = array[i];
      array[i] = array[j];
      array[j] = temp;
      System.out.println("[" + i + "]" + " = " + array[i]);
    }
  }
  
}