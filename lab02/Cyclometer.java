//Yichen Shen Feb,06,2019
//CSE002 Lab 02 Cyclometer
//Found the number of minutes, counts, distances for each and both of the two trips

public class Cyclometer{
  //main method for every required java program
   public static void main(String[] args){
     //our input data
     int secsTrip1=480; // Time elapsed for Trip1 in seconds
     int secsTrip2=3220; //Time elasped for Trip2 in seconds
     int countsTrip1=1561; //Number of rotations of wheels for trip1
     int countsTrip2=9037; //Number of rotations of wheels for trip2
     
     //out intermediate variables and output data
     double wheelDiameter=27.0, //the diameter of the wheel in foot
     PI=3.14159, //The value for the constant number PI
     feetPerMile=5280, // 5280 feet = 1 mile
     inchesPerFoot=12, // 12 inches = 1 foot
     secondsPerMinute=60; //60 seconds = 1 minutes
     double distanceTrip1, distanceTrip2, totalDistance; //The distances travelled by trip1, trip2, and both of them together
       
     System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute)+ " minutes and had "+countsTrip1+" counts.");
     System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");
     
     //Run the calculations; store the values.
     //Find the distance of trip 1, trip 2 and the total distance of trip 1 and trip 2
     distanceTrip1=countsTrip1*wheelDiameter*PI;
     //Aboce gives distance in inches
     //For each count, a rotation of the wheel travels the diameter in inches times PI
     distanceTrip1/=inchesPerFoot*feetPerMile; //Gives distances for trip 1 in miles
     distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Gives distances for trip 2 in miles
     totalDistance=distanceTrip1+distanceTrip2;
     
     //printout the output data
     System.out.println("Trip 1 was "+ distanceTrip1+" miles.");
     System.out.println("Trip 2 was "+ distanceTrip2+" miles.");
     System.out.println("The total distance was "+totalDistance+" miles.");
   } //end of main method
} //end of class