//CSE002 Yichen Shen lab09
//Searching
//experience implementing methods that accepts arrays
//experience implementing array searches

import java.util.Arrays;
import java.util.Scanner;

public class Searching{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);//declare a scanner
    System.out.print("linear search press 1; binary search press 2: ");
    int user = myScanner.nextInt();
    System.out.println();//make antoherl ine
    
    if(user == 1){//perform linear search
      System.out.print("Enter the array size and an integer search term: ");
      int arraySize = myScanner.nextInt();//accept the array size
      int integer = myScanner.nextInt();//accpet the integer to search
      int[] array = generateArray(arraySize);//generate an array
      int index = linearSearch(array,integer);
      for(int i=0;i<array.length;i++){
        System.out.println(array[i]);
      }
      System.out.println("The index is: " + index);
    }
    else if(user == 2){//perform binary search
      System.out.print("Enter the array size and an integer search term: ");
      int arraySize = myScanner.nextInt();//accept the array size
      int integer = myScanner.nextInt();//accpet the integer to search
      int[] array = ascendingArray(arraySize);//generate an array
      int index = binarySearch(array,integer);
      for(int i=0;i<array.length;i++){
        System.out.println(array[i]);
      }
      System.out.println("The index is: " + index);
      
    }
    
    
  }
  
  public static int[] generateArray(int size){
    int[] array = new int[size];//declare and allocate the array
    
    for(int i = 0 ; i<size;i++){
      array[i] = (int)(Math.random()*(size+1));//assign the array with random integers from 0 to the size
      
    }
    
    return array;//return the array
    
  }//method to generate the array
  
  public static int[] ascendingArray(int size){
   int[] array = new int[size];
    for(int i =0; i<size;i++){
      array[i] = (int)(Math.random()*(size+1));//assign the array with random integers from 0 to the size
    }
    Arrays.sort(array);//sort the array
    return array;
    
   }//method to generate the ascending array
  
  public static int linearSearch(int[] input, int search){
    int i = 0; //index
    for(i = 0; i<input.length;i++){
      if(input[i] == search){
        return i;//return the index
      }
    }
    return -1;//if there is no single integer, return -1
  }//method for linear search
  
  public static int binarySearch(int[] input, int search){
    int i = 0;//index
    
    int initialIndex = 0;
    int finalIndex = input.length-1;
    int searchingIndex = (finalIndex + initialIndex)/2;//the middle index
    
    System.out.println("InitialIndex = " + initialIndex + " FinalIndex = " + finalIndex + " Searching index = "+ searchingIndex);
    System.out.println();
    
    for(i = 0; finalIndex > initialIndex;i++){
      
    
      if(input[searchingIndex] == search){
        return searchingIndex;
      }
      else if(input[searchingIndex] < search){
        initialIndex = searchingIndex+1;//the initial value change to the middle index
      
      }
      else if(input[searchingIndex] > search){
        finalIndex = searchingIndex-1;//set the final index to the searchingIndex
     }
      searchingIndex = (finalIndex + initialIndex)/2;//set the new the middle index
      System.out.print(" initialindex = " + initialIndex + " finalindex = " + finalIndex);
      System.out.print(" i=" + i + " searcingIndex = " + searchingIndex);
      System.out.println(" input[" + searchingIndex + "] = " + input[searchingIndex] + " integer = " + search );

    }
    
    return -1;//did not find the value
    
    
  }//binary searchig method
  

  
  
}