//YiChen Shen Feb,11,2019
//CSE002 HW03 Program 1
//Convert meter into inches

import java.util.Scanner; //import the scanner class

public class Convert{
  //main method required for every java program
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //declare an instance and call scanner Constructor
    System.out.print("Enter the distance in meters: "); //prompt the user to enter the distance in meters 
    double distanceInMeters = myScanner.nextDouble(); //accept the user input
    double distanceInInches = 39.3701 * distanceInMeters; //1 meter = 39.3701 inches, convert meters into inches
    //distanceInInches = (int)(distanceInInches*10000);//remain four decimal places
   //distanceInInches /=10000.0;//get the original value with four decimal places
    distanceInInches = (int)(distanceInInches*10000)/10000.0; //remain four decimal places
    System.out.println( distanceInMeters + " meters is " + distanceInInches + " inches. "); //print out the answer
    
  }//end of  main method
} //end of class
