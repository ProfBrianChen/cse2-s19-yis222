//Yichen Shen Feb,11,2019
//CSE002 HW03 Program 2
//Calculate the volume of the box, given length, width and height as input

import java.util.Scanner;//import the scanner class

public class BoxVolume{
  //every java program requires a main method
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //declare an instance
    System.out.print("The width side of the box is: "); //prompt the user to enter the width
    double width = myScanner.nextDouble(); //accept the box's width 
    System.out.print("The length of the box is: "); //prompt the user to enter the length
    double length = myScanner.nextDouble();//accept the box's length
    System.out.print("The height of the box is: ");//prompt the user to enter the height
    double height = myScanner.nextDouble();//accept the box's height
    double volume = width * length * height; //calculate the volume by multiplying width, length and height 
    System.out.println("The volume inside the box is: " + volume); //print out the volume of the box
   
  }//end of main method
}//end of class
