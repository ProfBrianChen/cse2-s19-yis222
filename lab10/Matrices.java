import java.util.Scanner;

public class Matrices{
  public static void main(String[] args){
    int heighta = (int)(Math.random()*6+3);
    int widtha = (int)(Math.random()*6+3);
    int heightb = (int)(Math.random()*6+3);
    int widthb = (int)(Math.random()*6+3);
    int[][] A = increasingMatrix(heighta,widtha,true);
    int[][] B = increasingMatrix(heighta,widtha,false);
    int[][] C = increasingMatrix(heightb,widthb,true);
    System.out.println("MATRIX A: ");
    printMatrix(A,true);
    System.out.println("MATRIX B: ");
    printMatrix(B,false);
    System.out.println("MATRIX C: ");
    printMatrix(C,true);
    
   
    System.out.println("MATRIX A+B: ");
    int[][] sum1 = addMatrix(A,true,B,false);
    printMatrix(sum1,true);
    System.out.println("MATRIX A+C: ");    
    int[][] sum2 = addMatrix(A,true,C,true);
    printMatrix(sum2,true);
    
   
  }//main method
  public static int[][] increasingMatrix(int height, int width, boolean format){
    int[][] matrix;
    int value = 1;
    if(format == true){//row-major representation
      matrix = new int[height][width];
      for(int i = 0; i<height; i++){
        for(int j = 0; j<width;j++){
          matrix[i][j] = value;
          value ++;
        }
        System.out.println();//another line
      }
    }
    else{//column-major representation
      matrix = new int[width][height];
      for(int i = 0; i<height; i++){
        for(int j = 0; j<width;j++){
          matrix[j][i] = value;
          value++;
        }
        System.out.println();
        
      }
    }
    return matrix;
    
    
  }//increasingMatrix method
  
  public static void printMatrix(int[][] array, boolean format){
    if(format == true){//row-major representation
      for(int row = 0; row<array.length; row++){
        System.out.print("[ ");
        for(int column = 0; column<array[row].length; column++){//print out row by row
          System.out.print(array[row][column] + "\t");
        }
        System.out.println("]");//make another line
      }
    }
    else{//column-major representation
      for(int row = 0; row<array[0].length;row++){
         System.out.print("[ ");
       
        for(int column = 0; column<array.length; column++){
          System.out.print(array[column][row] + "\t");
        }
        System.out.println("]");//make another line
      }
      
    }
    
  }//printMatrix method
  
  public static int[][] translate(int[][] columnArray){//assume the input is a column major format
    int height = columnArray[0].length;
    int width = columnArray.length;
    int[][] rowArray = new int[height][width];
    for(int i = 0; i<height;i++){
      for(int j = 0; j<width; j++){
        rowArray[i][j] = columnArray[j][i];
      }
    }
    return rowArray;
  }//translate method
  
  public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
    //assess wether the arrays can be added
    int heighta = 0;
    int widtha = 0;
    int heightb = 0;
    int widthb = 0;
    
    if(formata == true){
      heighta = a.length;
      widtha = a[0].length;
    }
    else if(formata == false){
      heighta = a[0].length;
      widtha = a.length;
      a = translate(a);//translate to a row-major format
      
    }
    if(formatb == true){
      heightb = b.length;
      widthb = b[0].length;
    }
    else if(formatb == false){
      heightb = b[0].length;
      widthb = b.length;
      b = translate(b);//translate to a row-major format
    }
    
    if(heighta != heightb || widtha != widthb){//if they do not have the same length or width
      System.out.println("They cannot be added!");
      return null;
    }
    
    //add
    int[][] sum = new int[heighta][widtha];
    for(int i = 0; i<heighta; i++){
      for(int j = 0; j<widtha; j++){
        sum[i][j] = a[i][j] + b[i][j];
      }
    }
    return sum;
    
    
  }//addMatrix method
  
}//main class