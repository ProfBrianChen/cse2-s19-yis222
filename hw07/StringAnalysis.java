//CSE002 HW07 Yichen Shen
//method
//check whether a string is all letters, or just a specified number of characters in the string

import java.util.Scanner;

public class StringAnalysis{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter a string: ");
    String inputString = myScanner.next();//accept the string from the user
    System.out.print("Do you want to check the whole string?  yes/no: ");//ask whether the user want to check all or just specific numbers
    String check = new String(myScanner.next());//accpet the answer from the user
    Boolean answer = true;
    if(check.equals("yes")){//if the user want to check all the characters
     answer = examining(inputString);//call the method
      if(answer == true){
        System.out.println("The String is all letters!");
      }
      else{
        System.out.println("The String is not all letters!");
      }
    }
    else{
      System.out.print("Enter the number of characters you want to exam in the string: ");//prompt the user to type in the number of characters he or she wants to exam
      int number = myScanner.nextInt();//accept the number from the user
      answer = examining(inputString,number);//call the method
      if(answer == true){
        System.out.println("The String is all letters!");
      }
      else{
        System.out.println("The String is not all letters!");
      }
      
    }
    
    
  }//main method
  
  public static boolean examining(String input){
    int i = 0;//the index
    while(i<input.length()){
      if(input.charAt(i) < 'a' || input.charAt(i) > 'z'){//if the character is not a letter
        return false;//return false
      }
      i++;//increase one to i
    }
    return true;//if all the characters are the letters, return true 
  }//method to ehcek the string
  
  public static boolean examining(String input, int number){
    int i = 0;//the index 
    
    if(input.length()<=number){//if the number is greater thant the total numbers of characters
      number = input.length();//assign the length to the number or call the method
    }
    
    while(i<number){//while the index is less than the number, do the checking process
      if(input.charAt(i)<='a' || input.charAt(i) >= 'z'){//if the character is not a letter
        return false;//return false
      }
      i++;//plus one to the i
    }
    return true;//if all the characters are letters, return true
  }//method to check the string and the number position
}//main class