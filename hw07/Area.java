//CSE002 hw07 Yichen Shen Program 1
//method
//ask the user to input the shape and dimention of the shape, and calculate the area

import java.util.Scanner;//import the scanner class


public class Area{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("This helps you calculate the area of three shapes.");
    System.out.print("Choose the shape among triangle, rectangle and circle: ");//prompt the user to input the shape
    String input = new String(myScanner.next());//accept the value from the user
    
    //check whether the user input the valid shape
   while(true){
     if(input.equals("triangle")){
       triangle();
       break;
     }
     else if(input.equals("rectangle")){
       rectangle();
       break;
     }
     else if(input.equals("circle")){
       circle();
       break;
     }
     else{
       System.out.print("You did not input the valid shape. Choose the shape among triangle, rectangle and circle:");//ask again
       input = myScanner.next();
     } 

   }
    
    
    
  }//main method
  
  public static void triangle(){    
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the base in double: ");
    double base = checkDouble();
    System.out.print("Enter the height in double: ");
    double height = checkDouble();
    double area = 0.5 * base * height;
    System.out.println("The area of the triangle is " + area);
    
  }// method to calculate the area of the triangle
  
  
  public static void rectangle(){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the base in double: ");
    double base = checkDouble();
    //double base = myScanner.nextDouble();
    System.out.print("Enter the height in double: ");
    //double height = myScanner.nextDouble();
    double height = checkDouble();
    double area = base * height;
    System.out.println("The area of the rectangle is " + area);
    
  }//method to calculate the area of the rectangle
  
  public static void circle(){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the radius in double: ");
    double radius = checkDouble();
    double area = Math.PI * radius * radius;
    System.out.println("The area of the circle is " + area);
  
  }//method to calculate the area of the circle
  
  public static double checkDouble(){
    Scanner myScanner = new Scanner(System.in);
    while(myScanner.hasNextDouble() == false){
      System.out.print("Error. try again: ");
      myScanner.next();
    }
    double result = myScanner.nextDouble();
    return result;
  }//method to check whether the values the user input are doubles
  
}//main class
