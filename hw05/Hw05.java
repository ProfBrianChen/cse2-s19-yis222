//Yichen Shen CSE HW05
//Goal: asks the user to enter information relating to a course they are currently taking
//Learning purpose: Learn how to use loop

import java.util.Scanner; //import the scanner class

public class Hw05{ //main class
  //main method
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //declare an instance for scanner
    
    //ask for the course number
    System.out.print("Enter the course number: "); //prompt the user to enter an integer
    while (myScanner.hasNextInt() == false){ //if the user did not enter an integer
      System.out.print("Error. You need input of integer type, please try again: "); //show an error, and ask again
      myScanner.next(); //remove the junk data
     
    }
    int courseNumber = myScanner.nextInt();//assign the integer to courseNumber
    
   //ask for the department name 
    System.out.print("Enter the department name: "); //prompt the user to enter a string
    while (myScanner.hasNext() == false){ //if use did not enter a string
      System.out.print("Error. You need input of string type, please try again: "); //show an error, and ask again
      myScanner.next();//remove the junk data
    }
    String departmentName = myScanner.next(); //assign the string to departmentName
    
    //ask for the times it meets in a week
    System.out.print("Enter the number of times it meets in a week: "); //prompt the user to enter an integer
    while (myScanner.hasNextInt() == false){ // if user did not enter an integer
      System.out.print("Error. You need input an integer type, please try again: "); // show an error, and ask again
      myScanner.next();//remove the junk data
    }
    int numberOfTimesMeet = myScanner.nextInt(); //assign the integer to numberOfTimesMeet
    
    //ask for the time it starts by asking hours and minutes separately
    System.out.print("Enter the hour the class starts at: "); //prompt the user to enter an integer
    while (myScanner.hasNextInt() == false ){ // if user did not enter an integer
      System.out.print("Error. You need input an integer type, please try again: "); // show an error, and ask again
      myScanner.next();//remove the junk data

    }
     int hour = myScanner.nextInt(); //assign the integer to hour
    
     System.out.print("Enter the minute the class starts at: "); //prompt the user to enter an integer
     while (myScanner.hasNextInt() == false ){ // if user did not enter an integer
      System.out.print("Error. You need input an integer type, please try again: "); // show an error, and ask again
      myScanner.next();//remove the junk data

    }
     int minute = myScanner.nextInt(); //assign the integer to minute

    
    //ask for the instructor's name
    System.out.print("Enter the instructor's name: ");//prompt the user to enter a string
    while(myScanner.hasNext() == false){//if the user did not enter a string
      System.out.print("Error. You need input a string type, please try again: "); //show an error, and ask again
      myScanner.next(); //flush out the junk data
    }
    String instructorName = myScanner.next();//assign the string to instructorName
    
    //ask for the number of students
    System.out.print("Enter the number of sudents in the class: ");//prompt the user to enter an integer
    while(myScanner.hasNextInt() == false){//if the user did not enter an integer
      System.out.print("Error. You need input an integer type, please try agasin: "); //show an error, and ask again
      myScanner.next();//remove the junk data
    }
    int numberOfStudents = myScanner.nextInt();//assign an integer to numberOfStudents
    
    //print out the final results to check
    System.out.println("Course number: " + courseNumber);
    System.out.println("Department name: " + departmentName);
    System.out.println("The number of times the class meets: " + numberOfTimesMeet);
    System.out.println("Time starts : " + hour + minute);
    System.out.println("Instructor's name : " + instructorName);
    System.out.println("Number of students: " + numberOfStudents);

    
  }
}