//Yichen Shen CSE002 Extra credit hw05
//use loops to print out the argyle shape

public class Argyle{
  public static void main(String[] args){
    int heightIndex = 0;
    int widthIndex = 0;
    
    int widthIndex2= 0;
    int heightIndex2 = 0;
    
    
    int lengthOfViewingWindow = 85;
    int widthOfViewingWindow = 20;
    int widthOfArgyleDiamond = 8;
    
    int widthOfCenterStrip = 5;
    
    int i = 0;
    
    
    
    while(i<lengthOfViewingWindow){
      
      heightIndex = i%(widthOfArgyleDiamond);
      heightIndex2 = i%(widthOfArgyleDiamond*2);
      
      for(int j = 0; j<widthOfViewingWindow; j++){//print out .
        widthIndex = j%(widthOfArgyleDiamond);
        widthIndex2 = j%(widthOfArgyleDiamond*2);

        
        //upper left corner
       if(widthIndex2<widthOfArgyleDiamond && heightIndex2 < widthOfArgyleDiamond){//if it is upper left corner
        if(widthIndex >= heightIndex-1 && widthIndex < heightIndex+widthOfCenterStrip-1){//if it fits the strip
          System.out.print("#");//print out the strip
          
          }
       else if(widthIndex<(widthOfArgyleDiamond-heightIndex)){//if it fits the first symbol
         System.out.print(".");//print out the first symbol
         }
        
        else{//if it fits the second symbol
          System.out.print("+");//print out the second symbol
        }
       }
        
        else if(heightIndex2 <= widthOfArgyleDiamond && widthIndex2 > widthOfCenterStrip){//if it upper right corner
          if(widthIndex <= (widthOfArgyleDiamond-heightIndex) && widthIndex > widthOfArgyleDiamond-heightIndex-widthOfCenterStrip ){
            System.out.print("#");
          }
          else if(widthIndex>=heightIndex){
            System.out.print(".");
          }
          else{
            System.out.print("+");
          }  
        }
        
        else if(heightIndex2 > widthOfArgyleDiamond && widthIndex2 <= widthOfArgyleDiamond){
          if(widthIndex <= (widthOfArgyleDiamond-heightIndex) && widthIndex > widthOfArgyleDiamond-heightIndex-widthOfCenterStrip ){
            System.out.print("#");
          }
          else if(widthIndex>heightIndex && widthIndex<=widthOfArgyleDiamond-heightIndex-widthOfCenterStrip){
            System.out.print("+");
          }
          else{
            System.out.print(".");
          }
 
        
      }
      
    
      
      }
        System.out.println();
      i++;
    }
    
  }
  
}