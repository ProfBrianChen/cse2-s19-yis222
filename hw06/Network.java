//Yichen Shen CSE002 HW05
//Make a network patern by using loops!

import java.util.Scanner; //import a scanner class

public class Network{
  //main class
  public static void main(String[] args){
    //main method
    
    //ask the user to input
    Scanner myScanner = new Scanner(System.in);//delcare my scanner
    
    int height = 0;//declare the height
    int width = 0;//declare the width
    int squareSize = 0;//declare the square size
    int edgeLength = 0;//declare the edge length
    
    //check for the height
    System.out.print("Input your desire height: ");//prompt the user to input the height
    while(true){
    while(myScanner.hasNextInt() == false){
      System.out.print("Error: Please type in an integer: ");//show an error if the input is not an integer
      myScanner.next();//flush out the junk data
    }
      height = myScanner.nextInt();
      if(height>0){
        break;//if the input is a positive integer, then break out of the loop
      }
      System.out.print("Error: Please type in a positve number: ");//show an error if the input is not a positive number
    }
    
   //check for the width
    System.out.print("Input your desire width: ");//prompt the user to input the height
    while(true){
    while(myScanner.hasNextInt() == false){
      System.out.print("Error: Please type in an integer: ");//show an error if the input is not an integer
      myScanner.next();//flush out the junk data
    }
      width = myScanner.nextInt();
      if(width>0){
        break;//if the input is a positive integer, then break out of the loop
      }
      System.out.print("Error: Please type in a positve number: ");//show an error if the input is not a positive number
    }
    
    //check for the square size
    System.out.print("Input your desire square size: ");//prompt the user to input the height
    while(true){
    while(myScanner.hasNextInt() == false){
      System.out.print("Error: Please type in an integer: ");//show an error if the input is not an integer
      myScanner.next();//flush out the junk data
    }
      squareSize = myScanner.nextInt();
      if(squareSize>0){
        break;//if the input is a positive integer, then break out of the loop
      }
      System.out.print("Error: Please type in a positve number: ");//show an error if the input is not a positive number
    }
     
    //check for the edge length
    System.out.print("Input your desire edge length: ");//prompt the user to input the height
    while(true){
    while(myScanner.hasNextInt() == false){
      System.out.print("Error: Please type in an integer: ");//show an error if the input is not an integer
      myScanner.next();//flush out the junk data
    }
      edgeLength = myScanner.nextInt();
      if(edgeLength>0){
        break;//if the input is a positive integer, then break out of the loop
      }
      System.out.print("Error: Please type in a positve number: ");//show an error if the input is not a positive number
    }
    
    
   //print out the network
    int i = 0;
    int widthIndicator = 0;//indicator of width
    int heightIndicator = 0;//indicator of height
    int x = squareSize%2; //determine whether the squareSize is an odd or even number
    int edgeLine1 = 0;//the line which prints out - or | for both even and odd number of square size
    int edgeLine2 = 0;//the line which prints out - or | for only even number of square size
    
    if(x==0){//if it is an even number, set the edge line 1 and 2 to the middle of the square
      edgeLine1 =  (squareSize-1)/2;
      edgeLine2 = edgeLine1+1;
    }
    else{//if it is an odd number, set the edge line 1 to the middle of the square
      edgeLine1 = (squareSize-1)/2;
    }
//System.out.println(edgeLine1);
//System.out.println(edgeLine2);

    
    
  do{
    //print out different symbols for each line
    heightIndicator = i%(squareSize+edgeLength);
    
    for(int j = 0; j<width;j++){
      widthIndicator = j%(squareSize+edgeLength);
      //System.out.print(widthIndicator);
      //System.out.print(heightIndicator);

      //if widthIndicator belongs to the square part
      if(widthIndicator < squareSize){//if this is less than the square size
        if(heightIndicator <squareSize){//if this is the square part
        
        if(heightIndicator == 0 || heightIndicator == squareSize-1 ){//if this line belongs to the side of the square
          if(widthIndicator == 0 || widthIndicator == squareSize-1){//if it is at the corner
            System.out.print("#");//print out # as corner
          }
          else{//if it is not at the corner
            System.out.print("-");//print out - as sides
          }
          
        }
        else{//if this line is between the side of the square
          if(widthIndicator == 0 || widthIndicator == squareSize-1){//if it is on the side
            System.out.print("|");//print out |
          }
          else{//if it is within the square
            System.out.print(" ");
          }
          
        }
       }
        else{//if it is not in the square part
          if(x==0){//if squareSize is an even number
            if(widthIndicator == edgeLine1 || widthIndicator == edgeLine2){//if the widthIndicator is on the edge line
              System.out.print("|");//print out |
              
            }
            else{//if the widthIndicator is not on the edge line
              System.out.print(" ");//print out space
            }
            
          }
          else{//if squareSize is an odd number
            if(widthIndicator == edgeLine1){//if widthIndicator is on the edge line
              System.out.print("|");//print out |
            }
            else{//if it is not on the edge line
              System.out.print(" ");//print out space
            }
            
          }
          
        }
      }
      
      else{//if widthIndicator belongs to the edge part, not the square part
        if(x==0){//if the squareSize is an even number
          if(heightIndicator == edgeLine1 || heightIndicator == edgeLine2){//if it is on the edge line1 or line 2
            System.out.print("-");//print out edge line -
          }
          else{//if it is not on the edge line
            System.out.print(" ");//print out space betweeen the square
          }
         
        }
        else{//if the squareSize is an odd number
          if(heightIndicator == edgeLine1){//if it is on the edge line 1
            System.out.print("-");//print out the edge line
          }
          else{//if it is not on the edge line 1
            System.out.print(" ");//print out the space between the square
          }
          
        }
        
      }
      
      
    }
    System.out.println("");//move to the next line
    i++;//plus one to i to move to the next line
    
    
  }while(i<height);//print out lines until i reaches the height
  

    
  
    
    
    
    
    }
  }
