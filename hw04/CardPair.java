//Yichen Shen Feb,18,2019
//CSE002 HW04
//Choose five cards randomly, and display whether there are any pairs
//Learn to use selection statements, operators, and String manipulation

import java.lang.Math; //import math so that I can use Math.random()
//import java.util.Scanner; //import Scanner method so that I can input a number to test my program

//class
public class CardPair{
  //main method
  public static void main(String[] args){
    //Use scanner to manipulate inputs
    //Scanner input = new Scanner(System.in); //Declare a scanner object
    //System.out.print("Enter the first random number: "); //prompt the user to send an input
    //int randomNumber1 = input.nextInt(); //accept the input as the first random number
    //System.out.print("Enter the second random number: "); //prompt the user to send an input
    //int randomNumber2 = input.nextInt(); //accept the input as the second random number
    //System.out.print("Enter the third random number: "); //prompt the user to send an input
    //int randomNumber3 = input.nextInt(); //accept the input as the third random number
    //System.out.print("Enter the fourth random number: "); //prompt the user to send an input
    //int randomNumber4 = input.nextInt(); //accept the input as the fourth random number
    //System.out.print("Enter the fifth random number: "); //prompt the user to send an input
    //int randomNumber5 = input.nextInt(); //accept the input as the fifth random number
  
    
   int randomNumber1 = (int)(Math.random()*51+1);//Generate the first random number
   int randomNumber2 = (int)(Math.random()*51+1);//Generate the second random number
   int randomNumber3 = (int)(Math.random()*51+1);//Generate the third random number
   int randomNumber4 = (int)(Math.random()*51+1);//Generate the fourth random number
   int randomNumber5 = (int)(Math.random()*51+1);//Gererate the fifth random number
    
   String suit1 = ""; //Declare the suit for the first number
   String suit2 = ""; //Declare the suit for the second number
   String suit3 = ""; //Declare the suit for the third number
   String suit4 = ""; //Declare the suit for the fourth numbe
   String suit5 = ""; //Declare the suit for the fifth number
    
   String royalIdentity1 = ""; //Declare the identity for the first number
   String royalIdentity2 = ""; //Declare the identity for the second number
   String royalIdentity3 = ""; //Declare the identity for the thrid number
   String royalIdentity4 = ""; //Declare the identity for the fourth number
   String royalIdentity5 = ""; //Declare the identity for the fifth number
    
   int numberIdentity1 = 0; //Declare the number identity  for the first number
   int numberIdentity2 = 0; //Declare the number identity  for the first number
   int numberIdentity3 = 0; //Declare the number identity  for the first number
   int numberIdentity4 = 0; //Declare the number identity  for the first number
   int numberIdentity5 = 0; //Declare the number identity  for the first number
    
   int counter = 0; //Declare a counter to count the number of same cards' numbers

    
    System.out.println("Your random cards were: ");
    
    //Determine the suit and identity for the first random number
   if(randomNumber1 <= 13){
     suit1 = "Diamonds";
   }
    else if(randomNumber1 <= 26){
      suit1 = "Clubs";
    }
    else if(randomNumber1 <= 39){
      suit1 = "Hearts";
    }
    else if(randomNumber1 <= 52){
      suit1 = "Spades";
    }
    
    randomNumber1 = randomNumber1 % 13; //convert the random number within 1 to 13;
    
    switch (randomNumber1){
      case 1:
        royalIdentity1 = "Ace"; //assign Ace to royal identity
        break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
        numberIdentity1 = randomNumber1; //assign their number to number identity
        break;
      case 11:
        royalIdentity1 = "Jack"; //assign Jack to royal identity
        break;
      case 12:
        royalIdentity1 = "Queen"; //assign Queen to royal identity
        break;
      case 0:
        royalIdentity1 = "King";//assign King to royal identity
        break;
      default:
        royalIdentity1 = ""; //assign nothing to royal identity
        break;
    }
    
    if (randomNumber1 == 11 || randomNumber1 == 12 || randomNumber1 == 0 || randomNumber1 == 1){ //if the card belongs to royal cards
      System.out.println("the " + royalIdentity1 + " of " + suit1 ); //print out its royal identity and suit
    }
    else { // if the card does not belong to royal cards
      System.out.println("the " + numberIdentity1 + " of " + suit1); //print out its number identity and suit
    }
    
    
       //Determine the suit and identity for the second random number
   if(randomNumber2 <= 13){
     suit2 = "Diamonds";
   }
    else if(randomNumber2 <= 26){
      suit2 = "Clubs";
    }
    else if(randomNumber2 <= 39){
      suit2 = "Hearts";
    }
    else if(randomNumber2 <= 52){
      suit2 = "Spades";
    }
    
    randomNumber2 = randomNumber2 % 13; //convert the random number within 1 to 13;
    
    switch (randomNumber2){
      case 1:
        royalIdentity2 = "Ace"; //assign Ace to royal identity
        break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
        numberIdentity2 = randomNumber2; //assign their number to number identity
        break;
      case 11:
        royalIdentity2 = "Jack"; //assign Jack to royal identity
        break;
      case 12:
        royalIdentity2 = "Queen"; //assign Queen to royal identity
        break;
      case 0:
        royalIdentity2 = "King";//assign King to royal identity
        break;
      default:
        royalIdentity2 = ""; //assign nothing to royal identity 
        break;
    }
    
    if (randomNumber2 == 11 || randomNumber2 == 12 || randomNumber2 == 0 || randomNumber2 == 1){ //if the card belongs to royal cards
      System.out.println("the " + royalIdentity2 + " of " + suit2 ); //print out its royal identity and suit
    }
    else { // if the card does not belong to royal cards
      System.out.println("the " + numberIdentity2 + " of " + suit2); //print out its number identity and suit
    }
    
       //Determine the suit and identity for the third random number
   if(randomNumber3 <= 13){
     suit3 = "Diamonds";
   }
    else if(randomNumber3 <= 26){
      suit3 = "Clubs";
    }
    else if(randomNumber3 <= 39){
      suit3 = "Hearts";
    }
    else if(randomNumber3 <= 52){
      suit3 = "Spades";
    }
    
    randomNumber3 = randomNumber3 % 13; //convert the random number within 1 to 13;
    
    switch (randomNumber3){
      case 1:
        royalIdentity3 = "Ace"; //assign Ace to royal identity
        break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
        numberIdentity3 = randomNumber3; //assign their number to number identity
        break;
      case 11:
        royalIdentity3 = "Jack"; //assign Jack to royal identity
        break;
      case 12:
        royalIdentity3 = "Queen"; //assign Queen to royal identity
        break;
      case 0:
        royalIdentity3 = "King";//assign King to royal identity
        break;
      default:
        royalIdentity3 = ""; //assign nothing to royal identity 
        break;
    }
    
    if (randomNumber3 == 11 || randomNumber3 == 12 || randomNumber3 == 0 || randomNumber3 == 1){ //if the card belongs to royal cards
      System.out.println("the " + royalIdentity3 + " of " + suit3 ); //print out its royal identity and suit
    }
    else { // if the card does not belong to royal cards
      System.out.println("the " + numberIdentity3 + " of " + suit3); //print out its number identity and suit
    }
    
       //Determine the suit and identity for the fourth random number
   if(randomNumber4 <= 13){
     suit4 = "Diamonds";
   }
    else if(randomNumber4 <= 26){
      suit4 = "Clubs";
    }
    else if(randomNumber4 <= 39){
      suit4 = "Hearts";
    }
    else if(randomNumber4 <= 52){
      suit4 = "Spades";
    }
    
    randomNumber4 = randomNumber4 % 13; //convert the random number within 1 to 13;
    
    switch (randomNumber4){
      case 1:
        royalIdentity4 = "Ace"; //assign Ace to royal identity
        break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
        numberIdentity4 = randomNumber4; //assign their number to number identity
        break;
      case 11:
        royalIdentity4 = "Jack"; //assign Jack to royal identity
        break;
      case 12:
        royalIdentity4 = "Queen"; //assign Queen to royal identity
        break;
      case 0:
        royalIdentity4 = "King";//assign King to royal identity
        break;
      default:
        royalIdentity4 = ""; //assign nothing to royal identity 
        break;
    }
    
    if (randomNumber4 == 11 || randomNumber4 == 12 || randomNumber4 == 0 || randomNumber4 == 1){ //if the card belongs to royal cards
      System.out.println("the " + royalIdentity4 + " of " + suit4 ); //print out its royal identity and suit
    }
    else { // if the card does not belong to royal cards
      System.out.println("the " + numberIdentity4 + " of " + suit4); //print out its number identity and suit
    }
    
       //Determine the suit and identity for the fifth random number
   if(randomNumber5 <= 13){
     suit5 = "Diamonds";
   }
    else if(randomNumber5 <= 26){
      suit5 = "Clubs";
    }
    else if(randomNumber5 <= 39){
      suit5 = "Hearts";
    }
    else if(randomNumber5 <= 52){
      suit5 = "Spades";
    }
    
    randomNumber5 = randomNumber5 % 13; //convert the random number within 1 to 13;
    
    switch (randomNumber5){
      case 1:
        royalIdentity5 = "Ace"; //assign Ace to royal identity
        break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
        numberIdentity5 = randomNumber5; //assign their number to number identity
        break;
      case 11:
        royalIdentity5 = "Jack"; //assign Jack to royal identity
        break;
      case 12:
        royalIdentity5 = "Queen"; //assign Queen to royal identity
        break;
      case 0:
        royalIdentity5 = "King";//assign King to royal identity
        break;
      default:
        royalIdentity5 = ""; //assign nothing to royal identity 
        break;
    }
    
    if (randomNumber5 == 11 || randomNumber5 == 12 || randomNumber5 == 0 || randomNumber5 == 1){ //if the card belongs to royal cards
      System.out.println("the " + royalIdentity5 + " of " + suit5 ); //print out its royal identity and suit
    }
    else { // if the card does not belong to royal cards
      System.out.println("the " + numberIdentity5 + " of " + suit5); //print out its number identity and suit
    }
    
    
    //Start compare them
    if(randomNumber1 == randomNumber2){ //if the first number euqls to the second number
      counter += 1; //add one to the counter
    }
    if(randomNumber1 == randomNumber3) { // if the first number equals to the third number
      counter += 1;//add one to the counter
    }
    if(randomNumber1 == randomNumber4) {// if the first number equals to the fourth number
      counter += 1;//add one to the counter
    }
   if(randomNumber1 == randomNumber5) {// if the first number equals to the fifth number
      counter += 1;//add one to the counter
    }
    if(randomNumber2 == randomNumber3) {// if the second number equals to the third number
      counter += 1;//add one to the counter
    }
    if(randomNumber2 == randomNumber4) {// if the second number equals to the fourth number
      counter += 1;//add one to the counter
    }
    if(randomNumber2 == randomNumber5) {// if the second number equals to the fifth number
      counter += 1;//add one to the counter
    }
    if(randomNumber3 == randomNumber4) {// if the third number equals to the fourth number
      counter += 1;//add one to the counter
    }
    if(randomNumber3 == randomNumber5) {// if the third number equals to the fifth number
      counter += 1;//add one to the counter
    }
   if(randomNumber4 == randomNumber5) {// if the fourth number equals to the fifth number
      counter += 1;//add one to the counter
    }
    
    //System.out.println("Counter equals to " + counter);
    System.out.println(""); //space a line
    
    //print out the final conclusion
    if(counter == 0){
      System.out.println("You have a high Card hand!"); 
    }
    else if(counter == 1 || counter == 3){
      System.out.println("You have a pair!");
    }
    else if(counter == 2 || counter == 4 || counter == 10){
      System.out.println("You have two pairs!");
    }
   
   
    
  }//end of main method
}//end of class